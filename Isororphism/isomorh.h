#ifndef ISOMORH_H
#define ISOMORH_H

#include <vector>
#include <map>

using namespace std;

bool calculate(vector<vector<int> > rgraphA, vector<vector<int> > rgraphB);
vector<vector<int> > dijkstra(vector<vector<int> > graph);
vector<vector<int> > reindex(vector<vector<int> > graph, vector<int> index);
vector<int> inv(vector<int> index);
vector<int> transform(map<vector<int>,vector<int> > signmatrixA,
                      map<vector<int>,vector<int> > signmatrixB, vector<int> vertexA,
                      vector<int> vertexB, vector<int> isoB);

#endif // ISOMORH_H
