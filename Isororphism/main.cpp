#include <iostream>
#include <map>
#include <vector>
#include <memory>
#include <tuple>

#include <atomic>
#include <thread>
#include <future>
#include <mutex>

#include <QDebug>
#include <QString>
#include <QDir>
#include <QFile>
#include <QDataStream>
#include <QTextStream>
#include <QByteArray>
#include <QThread>

#include "isomorh.h"
#include "comparator.h"

using namespace std;
typedef std::tuple<std::vector<std::vector<int>>, bool, int> matrix;

//функция разбивающая исходную последовательность по размеру сета на разные файлы
void prepare(int n) {
    QDir dir("tmp");
    if(!dir.exists()) {
        qDebug() << (dir.mkdir("tmp") ? "dir created" : "dir could not be created");
    }
    qDebug() << dir.absolutePath();

    map<int, shared_ptr<QFile>> _files;
    std::vector<int> counters(n,0);

    QFile input("C:\\Qt_projects\\Reach60\\build-MPITestSD-Desktop_Qt_5_11_1_MinGW_32bit-Debug\\Isororphism\\out.txt");
    input.open(QIODevice::ReadOnly);
    if(!input.isOpen()) {
        qDebug() << "Could not open input file";
        exit(1);
    }
    while(!input.atEnd()) {
        auto line = input.readLine();
        QTextStream stream(&line, QIODevice::ReadOnly);

        int count = 0;
        int last = 0;
        while(!stream.atEnd()) {
            int ch = 0;
            stream >> ch;
            if(ch == 0) break;
            count++;
            last = ch;
        }
        int fileName = 0;
        if(last == n/2) fileName = count*2-1;
        else fileName = count*2;
        if(_files.find(fileName) == _files.end()) {
            shared_ptr<QFile> newFile = shared_ptr<QFile>(new QFile("tmp/"+QString::number(fileName)+".txt"));
            newFile->open(QIODevice::WriteOnly);
            if(!newFile->isOpen()) {
                qDebug() << "Could not open tmp file";
                exit(2);
            }
            _files[fileName] = newFile;
        }
        QString data;
        data += line;
        _files[fileName].get()->write(data.toLatin1());

        counters[fileName]++;
    }

    QFile f("tmp/counters.txt");
    f.open(QIODevice::WriteOnly);
    for(size_t i = 0; i < counters.size(); ++i) {
        if(counters[i] != 0){
            f.write(QString::number(counters[i]).toLatin1().data());
            f.write("\n");

        }
    }
}

//функция преобразования сета в матрицу
std::vector<std::vector<int>> setToMatrix(QByteArray& data, int n) {
    //parse
    std::vector<std::vector<int>> result;
    {
        std::vector<int> line(n);
        for(int i = 0; i < n; ++i) {
            result.push_back(line);
        }
    }

    QTextStream stream(&data, QIODevice::ReadOnly);
    while(!stream.atEnd()) {
        int val = 0;
        stream  >> val;

        for(int i = 0; i < n; ++i) {
            result[i][(i+val)%n] = 1;
            result[(i+val)%n][i] = 1;
        }
    }

    return result;
}

std::atomic_uint currentLineChecking(0);
std::vector<matrix> _matrices = std::vector<matrix>();

void compare_metrices() {
    while(true) {
        auto startIndex = currentLineChecking++;
        auto size = _matrices.size();

        if(startIndex >= size) return;

        std::vector<std::vector<int>> matrix1;
        bool isValid;
        int index;
        std::tie(matrix1, isValid, index) = _matrices[startIndex];
        if(!isValid) continue;

        for(size_t i = startIndex+1; i < size; ++i) {
            if(std::get<1>(_matrices[i])) {
                auto equal = calculate(matrix1, std::get<0>(_matrices[i]));
                if(equal) {
                    std::get<1>(_matrices[i]) = false;
                    std::cout << std::get<2>(_matrices[i]) << " | " << index << " equal\n";
                }
            }
        }
    }
}

void compare(std::vector<matrix>& matrices) {
    currentLineChecking = 0;
    _matrices.swap(matrices);
    if(matrices.size() <= 1) return;
    std::vector<std::shared_ptr<std::thread>> threads;
    for(size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
        //        QThread thread = std::shared_ptr<QThread>(new QThread(compare_metrices));
        auto thread = std::make_shared<std::thread>(compare_metrices);

        threads.push_back(thread);
    }

    for(size_t i = 0; i < threads.size(); ++i) {
        threads[i].get()->join();
    }
}

int main()
{
    int n = 16;
    prepare(n);
    std::cout << "finished\n";
    exit(0);

    QDir dir("tmp");
    if(!dir.exists()) {
        qDebug() << "No such dir tmp";
        exit(3);
    }
    auto filesList = dir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    for(auto& fileName : filesList) {
        qDebug() << fileName;
        QFile f("tmp/"+fileName);
        f.open(QIODevice::ReadOnly);

        std::vector<matrix> matrices;
        size_t lineNumber = 0;
        while(!f.atEnd()) {
            auto line = f.readLine();
            matrix m = std::make_tuple(setToMatrix(line,n), true, lineNumber++);
            matrices.push_back(m);
        }
        compare(matrices);

        //вывод в файл остатков
        {
            f.seek(0);
            QFile out("tmp/out"+fileName+".txt");
            out.open(QIODevice::WriteOnly);

            size_t currentLine = 0;
            while(!f.atEnd()) {
                auto line = f.readLine();

                if(std::get<1>(_matrices[currentLine])) {
                    out.write(line);
                }
                ++currentLine;
            }
        }
    }
    qDebug() << "Finished!";

    return 0;
}
