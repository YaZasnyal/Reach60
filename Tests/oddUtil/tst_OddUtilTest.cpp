#include <QString>
#include <QtTest>
//#include <vector>

#include "../../oddCirculants/oddUtil.h"

class OddUtilTest : public QObject
{
    Q_OBJECT

public:
    OddUtilTest();

private Q_SLOTS:
    void createGeneratorTest();

    void testRotCompareEqual1();
    void testRotCompareEqual2();

    void testRotCompareNotEqual1();
};

OddUtilTest::OddUtilTest()
{
}

void OddUtilTest::createGeneratorTest()
{
    QVERIFY2(createGenerator(17) == 3, "createGeneratorTest:3");
}

void OddUtilTest::testRotCompareEqual1()
{
    std::vector<int> f,l;
    f = {1,2,3};
    l = {2,3,1};
    QVERIFY2(rotCompare(f, l), "rotCompare Equal1");
}

void OddUtilTest::testRotCompareEqual2()
{
    std::vector<int> f,l;
    f = {0,0,1,3};
    l = {0,1,3,0};
    QVERIFY2(rotCompare(f, l), "rotCompare Equal2");

}

void OddUtilTest::testRotCompareNotEqual1()
{
    std::vector<int> f,l;
    f={0,1,0,2};
    l={0,0,1,2};
    QVERIFY2(!rotCompare(f, l), "rotCompare notEqual");
}

QTEST_APPLESS_MAIN(OddUtilTest)

#include "tst_OddUtilTest.moc"
