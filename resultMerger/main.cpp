#include <algorithm>

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QString>

#include <QDir>

#include "FileIndexes.h"

class test {
public:
    test() {}
    test(int a) : _a(a) {}

private:
    int _a = 0;
};

int main(int argc, char *argv[])
{
    FileIndexes ind;

    QDir dir("../MPITest/out");
    auto files = dir.entryList();

    for(auto& it : files) {
        QFile f(dir.absoluteFilePath(it));
        f.open(QIODevice::ReadOnly);
        auto line = f.readLine();
        if(!ind.hasString(line)) {
            dir.rename(it, "../outNotMerged/"+it);
        }
    }
}
