#ifndef FILEINDEXES_H
#define FILEINDEXES_H

#include <vector>

#include <QVector>
#include <QFile>
#include <QDataStream>

//#include "indexData.h"

class indexData;

class FileIndexes : public std::vector<indexData>
{
public:
    FileIndexes();
    ~FileIndexes();

    void buildIndex();
    void saveIndexes();

    void addIndex(int64_t pos);
    QString getDataAt(int64_t pos);
    bool hasString(const QString& str);

    bool hasIndexes() const;
    void setHasIndexes(bool hasIndexes);

    friend QDataStream& operator<< (QDataStream& stream, FileIndexes& data);
    friend QDataStream& operator>> (QDataStream& stream, FileIndexes& data);

private:
    QFile _f;
    bool _hasIndexes = false;
};

class indexData {
public:
    indexData() {}
    indexData(int64_t pos) : _pos(pos) {}
    indexData(QString str) : _data(new QString(str)) {}
    indexData(const char* str) : _data(new QString(str)) {}

    int64_t _pos = 0;

    operator QString() /*const*/ {
        return getData();
    }
    operator const char*() /*const*/ {
        auto data = getData().toLatin1().data();
        return data;
    }

    QString getData() {
        if(!_indexes) {
            exit(2);
        }
        if(_data != 0 && _data->length() != 0) {
            return *_data;
        }
        _data = new QString(_indexes->getDataAt(_pos));
        return *_data;
    }

    static FileIndexes* _indexes;

    friend QDataStream& operator<<(QDataStream& stream, const indexData& data) {
        stream << data._pos;
        return stream;
    }
    friend QDataStream& operator>>(QDataStream& stream, indexData& data) {
        stream >> data._pos;
        return stream;
    }
private:
    QString *_data = 0;
};

#endif // FILEINDEXES_H
