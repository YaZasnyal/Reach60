#include "FileIndexes.h"
#include <QFile>
#include <QDebug>

FileIndexes* indexData::_indexes = 0;

FileIndexes::FileIndexes()
{
    indexData::_indexes = this;
    reserve(135662592);
    _f.setFileName("outs.txt");
    _f.open(QIODevice::ReadOnly);
    if(!_f.isOpen()) {
        exit(1);
    }

    QFile f("indexes.dat");
    bool ok = f.open(QIODevice::ReadOnly);
    if(ok) {
        QDataStream str(&f);
        str >> *this;
        _hasIndexes = true;
    }
    if(!hasIndexes()) {
        buildIndex();
    }
}

FileIndexes::~FileIndexes()
{

}

void FileIndexes::buildIndex()
{
    while(!_f.atEnd()) {
        QVector<char> buf;
        buf.resize(2048);
        int64_t posBeforeRead = _f.pos();
        auto readBytes = _f.read(buf.data(), buf.length());

        int64_t pos = 0;
        while(pos != -1) {
            pos = buf.indexOf('\n', pos);
            if (pos != -1) {
                ++pos;
                addIndex(posBeforeRead + pos);
            }
        }
    }
    saveIndexes();
}

void FileIndexes::saveIndexes()
{
    QFile f("indexes.dat");
    bool ok = f.open(QIODevice::WriteOnly);
    if(ok) {
        QDataStream str(&f);
        str << *this;
    } else {
        qDebug() << "Error while opening file for writing.";
    }
}

void FileIndexes::addIndex(int64_t pos)
{
    push_back(indexData(pos));
}

QString FileIndexes::getDataAt(int64_t pos)
{
    auto ok = _f.seek(pos);
    if(ok) {
        return _f.readLine().trimmed();
    }
    return QString();
}

bool FileIndexes::hasString(const QString& str)
{
    return std::binary_search(
                begin(),
                end(),
                str.toLatin1().data(),
                [](const char* first, const char* second) -> bool
    {
        QString a(first);
        QString b(second);
        bool res = a.trimmed() < b.trimmed();
        return res;
    });
}

bool FileIndexes::hasIndexes() const
{
    return _hasIndexes;
}

void FileIndexes::setHasIndexes(bool hasIndexes)
{
    _hasIndexes = hasIndexes;
}

QDataStream& operator<<(QDataStream& stream, FileIndexes& data) {
    stream << static_cast<int64_t>(data.size());
    for(auto& it : data) {
        stream << it._pos;
    }
    return stream;
}

QDataStream& operator>>(QDataStream& stream, FileIndexes& data) {
    int64_t dataSize = 0;
    stream >> dataSize;
    data.reserve(dataSize);
    for(int i = 0; i < dataSize; ++i) {
        int64_t value;
        stream >> value;
        indexData id(value);
        data.push_back(id);
    }
    return stream;
}
