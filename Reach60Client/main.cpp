//#include "MainWindow.h"
#include <QCoreApplication>

#include "Net/global.h"
#include "AbstractFactory.h"
#include "Consumer.h"
#include "Client.h"

//QString getArg(const QString& arg, const QString& defVal) {
//    auto args = QCoreApplication::arguments();
//    int id = args.indexOf(arg);
//    if(id == -1) {
//        return defVal;
//    }
//    return args[id+1];
//}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Client c(Factory<Consumer>());
    c.setPeerAddr(getArg("-ip", "35.228.42.70"));
    c.setThreadCount(getArg("-t", "-1").toInt());

    return a.exec();
}
