#-------------------------------------------------
#
# Project created by QtCreator 2018-07-17T23:25:14
#
#-------------------------------------------------

QT       += core network
QT -= gui
CONFIG += console c++14

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Reach60Client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../MPITest/src/Net/ \
               ../MPITest/src \

SOURCES += \
        main.cpp \
#        MainWindow.cpp \
    ../MPITest/src/Net/Client.cpp \
    ../MPITest/src/Net/Connection.cpp \
    ../MPITest/src/Net/Message.cpp \
    ../MPITest/src/Net/Server.cpp \
    ../MPITest/src/Net/ServerConnection.cpp \
    ../MPITest/src/Net/TaskRunnable.cpp \
    ../MPITest/src/Checker.cpp \
    ../MPITest/src/Consumer.cpp \
    ../MPITest/src/RecSubset.cpp \
    ../MPITest/src/taskGenerator.cpp \
    ../MPITest/src/taskSerializer.cpp \
    ../MPITest/src/util.cpp

HEADERS += \
#        MainWindow.h \
    ../MPITest/src/Net/AbstractFactory.h \
    ../MPITest/src/Net/Client.h \
    ../MPITest/src/Net/CommandsConst.h \
    ../MPITest/src/Net/Connection.h \
    ../MPITest/src/Net/Message.h \
    ../MPITest/src/Net/Server.h \
    ../MPITest/src/Net/ServerConnection.h \
    ../MPITest/src/Net/TaskRunnable.h \
    ../MPITest/src/Checker.h \
    ../MPITest/src/Consumer.h \
    ../MPITest/src/RecSubset.h \
    ../MPITest/src/taskGenerator.h \
    ../MPITest/src/taskSerializer.h \
    ../MPITest/src/util.h

#FORMS += \
#        MainWindow.ui

unix {
    LIBS += -L/usr/local/Cellar/gperftools/2.7/lib/ -ltcmalloc_minimal
}
