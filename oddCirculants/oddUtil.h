#ifndef ODDUTIL_H
#define ODDUTIL_H

#include <iostream>
#include <vector>

typedef std::vector<std::tuple<std::vector<int>, std::vector<int>>> tuples;
typedef std::tuple<std::vector<int>, std::vector<int>> newTuple;

//int order(int x, int n);
int powMod(int j, int i, int n);
int createGenerator(int n);
std::vector<int> createSequence(int w, int n);
std::vector<std::vector<int>> createTuples(int n);

bool rotCompare(std::vector<int> first, std::vector<int> last);

//Печать контейнеров
template <class T>
void printContainer(const T& c) {
    std::cout<<c.size() << ": ";
    for(auto& it : c) {
        std::cout<< it << " ";
    }
    std::cout << std::endl;
}

template <class T, class S>
void printContainer(S& stream, const T &c, bool newLine = true) {
    for(auto& it : c) {
        stream << it << " ";
    }
    if(newLine)
        stream << "\n";
}


#endif // ODDUTIL_H
