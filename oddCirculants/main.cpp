#include <iostream>
#include <vector>
#include <math.h>
#include <cmath>
#include <chrono>

#include <string>
#include <sstream>
#include <fstream>

#include "oddUtil.h"

int main()
{
//    printContainer(createSequence(3,17));
    auto start = std::chrono::steady_clock::now();
    auto res = createTuples(17);
    auto end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << "\n";

    std::ofstream out("out.txt");
    std::stringstream str;
    for(auto& it : res) {
        printContainer(str, it);
    }
    out << str.rdbuf();
    out.close();

    std::cout << " Finished!\n";
    return 0;
}
