TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    oddUtil.cpp

HEADERS += \
    oddUtil.h
