#include "oddUtil.h"
#include <tuple>
#include <numeric>
#include <vector>

//int order(int x, int n)
//{
//    static std::mutex _m;
//    static std::map<int, int> container;
//    {
//        auto foundSet = container.find(x);
//        if(foundSet != container.end()) {
//            return (*foundSet).second;
//        }
//    }

//    int it = 0;
//    while(true) {
//        if((((it) * x) % n) == 0) {
//            std::lock_guard<std::mutex> g(_m);
//            container[x] = it;
//            return (it);
//        }
//        ++it;
//    }

//    return -1;
//}
int powMod(int j, int i, int n) {
    if(i == 0) return 1;
    int jCopy = j;
    for(int k = 1; k < i; ++k) {
        jCopy = jCopy*j;
        if(jCopy > n) {
            jCopy %= n;
        }
    }
    return jCopy;
}

int createGenerator(int n) {
    for(int j = 2; j < n; ++j) {
        auto jCopy = j;
        int i = 1;
        while(true) {
            jCopy = jCopy*j;
            ++i;
            if(jCopy > n) {
                jCopy %= n;
            }
            if(jCopy == 1 && i < n-1) {
                break;
            } else if(jCopy == 1 && i == n-1) {
                return j;
            }
        }
    }
    return 0;
}

std::vector<int> createSequence(int w, int n) {
    std::vector<int> result(n-1);
    for(int i = 0; i < (n - 1)/2; ++i) {
        auto val = powMod(w,i,n);
        result[i] = val;
        result[(n-1)/2+i] = n-val;
    }
    return result;
}

bool rotCompare(std::vector<int> first, std::vector<int> last) {
    bool result = false;
    size_t size = first.size();
    for(size_t i = 0; i < size; ++i) {
        if(first[i] != last[0]) {
            continue;
        }
        for(size_t j = 0; j < size; ++j) {
            if(first[(i+j)%size] != last[j]) {
                break;
            } else {
                if(j == size-1) result = true;
            }
        }
    }
    return result;
}

void createTupleRec(tuples &res,
                    int k,
                    int _halfN,
                    newTuple nt = newTuple(),
                    int depth = 0)
{
    if(depth < k) {
        if(depth == 0) {
            std::get<0>(nt).resize(k);
            std::get<1>(nt).resize(_halfN+1);
        }
        int sum = std::accumulate(std::get<0>(nt).begin(), std::get<0>(nt).end(), 0);
        for(int i = 0; i <= _halfN; ++i) {
            if((sum + i) > _halfN) return;
            if(depth == k-1 && (sum + i) != _halfN) continue;
            auto ntCopy = nt;
            std::get<0>(ntCopy)[depth] = i;
            std::get<1>(ntCopy)[i]++;
            createTupleRec(res, k, _halfN, ntCopy, depth+1);
        }
        return;
    }
    //--------------------
    if(res.size() == 0) {
        res.push_back(nt);
        return;
    }
    bool write = true;
    for(auto& it : res) {
        if(std::get<1>(it) == std::get<1>(nt)) {
            if(rotCompare(std::get<0>(it), std::get<0>(nt))) {
                write = false;
            }
        }
    }
    if(write) res.push_back(nt);
}

std::vector<int> reformer(std::vector<int>& input, int n) {
    static auto sequence = createSequence(createGenerator(n), n);
    auto size = input.size();
    std::vector<int> result(size);
    result[0] = sequence[0];

    int counter = 0;
    for(size_t i = 1; i < size; ++i) {
        counter = counter+input[i-1]+1;
//        if(sequence[counter] > n/2) result[i] = n - sequence[counter];
        /*else*/ result[i] = sequence[counter];
//        result[i] = counter;
    }
    return result;
}

std::vector<std::vector<int> > createTuples(int n)
{
    std::vector<std::vector<int> > results;
    for(size_t k = 1; k <= (n-1)/2; ++k) {
        tuples t;
        createTupleRec(t, k, (n-1)/2-k); //
        auto size = t.size();
        for(size_t i = 0; i < size; ++i) {
            results.push_back(reformer(std::get<0>(t[i]), n));
        }
    }
    return results;
}
