#include <QCoreApplication>

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>
#include <QTimer>

#include <iostream>

#include <fstream>
#include <thread>
#include <future>
#include <unistd.h>
#include <chrono>

#include <string>

#include "src/util.h"
#include "src/taskGenerator.h"
#include "src/taskSerializer.h"

#include "Net/global.h"
#include "src/Checker.h"
#include "Net/Server.h"
#include "Net/Client.h"
#include "Generator.h"
#include "Consumer.h"
#include "Collector.h"

int main(int argc, char **argv)
{
    QCoreApplication a(argc, argv);

    auto args = a.arguments();
    Server s(0);
    Generator g(&s, getArg("-n", "10").toInt());
    Collector collector(g.getTotalTasks(), g.getN(), &a);
    collector.connect(&s, SIGNAL(resultRecieved(QByteArray)), &collector, SLOT(recievedData(QByteArray)), Qt::QueuedConnection);

    g.start();

    a.exec();
    return 0;
}
