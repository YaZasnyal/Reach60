#ifndef GENERATOR_H
#define GENERATOR_H

#include <QThread>
#include "Net/Client.h"

class Server;

class Generator : public QThread
{
    Q_OBJECT

public:
    Generator(Server* s, int n, QObject *parent = 0);
    Generator(Server* s, int n, int start, int end, QObject *parent = 0);
    void init();

    void run() override;

    int getTotalTasks() const;

    int getN() const;

private:
    void recCreate(int n, int index = 0, bool check = false);

    Server* _s;
    int _n;
    int totalTasks = 0;
    std::vector<int> v;
    std::vector<int> indexes;
    int ind;

    int _start = -1;
    int _end = -1;
};

#endif // GENERATOR_H
