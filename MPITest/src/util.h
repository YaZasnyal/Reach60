#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <set>
#include <vector>

std::set<int> divisors(int n /*= 60*/);
std::set<int> coprimes(int n /*= 60*/);
std::set<int> coprimesPlus(int n /*= 60*/);
std::set<int> meta(const std::set<int>& s, int n /*= 60*/);
/** @brief Meta + умножение на число по модулю n */
std::set<int> mult(int a, const std::set<int>& s, int n /*= 60*/);

/**
 * @brief Сравнивает два набора на изоморфизм.
 * @param first - set of type meta
 * @param last - set of type meta
 */
bool setCompare(const std::set<int>& first, const std::set<int>& last, int n /*= 60*/, bool debug = false);
/**
 * @brief Function that takes two sets and returns new set containing all common
 * elements
 * @param s1 - first set
 * @param s2 - second set
 * @return new set
 */
template <class T> std::set<T> setIntersection(const std::set<T>& s1,
                                               const std::set<T>& s2)
{
    std::set<T> result;
    for(auto it = s1.begin(); it != s1.end(); ++it) {
        if(s2.find((*it)) != s2.end()) {
            result.insert(*it);
        }
    }
    return result;
}
//std::set<int> setCreate(std::vector<int> )
std::set<int> setRange(int n /*= 60*/);

/** Порядок числа */
int order(int x, int n /*= 60*/);
std::set<int> elemsOfOrder(int k, const std::set<int> s, int n /*= 60*/);
std::set<int> elemsOfOrderPlus(int k, const std::set<int> s, int n /*= 60*/);
std::set<int> elemsOfOrderPlusK(int k, int n /*= 60*/);
std::vector<int> constants(int n /*= 60*/);

//Печать контейнеров
template <class T>
void printContainer(const T& c) {
    std::cout<<c.size() << ": ";
    for(auto& it : c) {
        std::cout<< it << " ";
    }
    std::cout << std::endl;
}

template <class T, class S>
void printContainer(S& stream, const T &c, bool newLine = true) {
    for(auto& it : c) {
        stream << it << " ";
    }
    if(newLine)
        stream << "\n";
}

#endif // UTIL_H
