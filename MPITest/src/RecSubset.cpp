#include "RecSubset.h"

RecSubset::RecSubset(const std::set<int>& s, int k) {
    subsets(s.begin(), s.end(), k);
}

std::vector<std::set<int> > RecSubset::getResult() const {
    return result;
}

void RecSubset::subsets(std::set<int>::const_iterator sBegin,
                        const std::set<int>::const_iterator& sEnd,
                        int k,
                        std::set<int> nextRes,
                        int index,
                        int enabled)
{
    if(sBegin != sEnd) {
        for(int i = 0; i <= 1; ++i) {
            if(i == 1) {
                nextRes.insert(*sBegin);
            }
            subsets(++sBegin, sEnd, k, nextRes, index+1, (i == 1 ? enabled+1 : enabled));
            --sBegin;
        }
        return;
    }
    if (enabled == k) {
        result.push_back(nextRes);
    }
}
