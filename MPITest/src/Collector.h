#ifndef COLLECTOR_H
#define COLLECTOR_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QElapsedTimer>
#include <QTimer>
#include <set>

class Collector : public QObject
{
    Q_OBJECT

public:
    Collector(int ntasks, int n, QObject *parent = 0);
    ~Collector() {
        qDebug() << "Destructor";
        str->flush();
        f->flush();
        f->close();
    }

public slots:
    void recievedData(QByteArray data);
    void printProgress();

private:
    QFile* f;
    QTextStream* str;
    QElapsedTimer elapsedTimer;
    QTimer timer;

    int _ntasks = -1;
    int _n;
    int writtenTasks = 0;
    bool _printFull = false;
};

#endif // COLLECTOR_H
