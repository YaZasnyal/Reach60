#ifndef TASKSERIALIZER_H
#define TASKSERIALIZER_H

#include <vector>
#include <set>

void serializeTask(std::vector<int>& buf, const std::vector<std::vector<std::set<int>>>& s);
void serializeResult(std::vector<int>& buf, std::set<std::set<int>> s);

std::vector<std::vector<std::set<int>>> deserialize(const std::vector<int>& buf);
//std::set<std::set<int>> deserializeResult(const std::vector<int>& buf);


template<class T>
void serializeResult(T& stream, std::set<std::set<int>> s) {
    stream << static_cast<int>(s.size());
    for(auto& it : s) {
        stream << static_cast<int>(it.size());
        for(auto& jt : it) {
            stream << static_cast<int>(jt);
        }
    }
}

template<class T>
std::set<std::set<int>> deserializeResult(T& stream) {
    std::set<std::set<int>> result;
    int value = 0;
    stream >> value;
    auto ss_size = value;
    for(int k = 0; k < ss_size; ++k) { //ss
        std::set<int> s;
        stream >> value;
        auto s_size = value;
        for(int k = 0; k < s_size; ++k) { //ss
            stream >> value;
            s.insert(value);
        }
        result.insert(s);
    }
    return result;
}

template<class T>
std::vector<std::vector<std::set<int> > > deserializeTask(T& stream) {
    std::vector<std::vector<std::set<int>>> result;
    int value = 0;
    stream >> value;
    auto vvs_size = value;
    for(int k = 0; k < vvs_size; ++k) { //vvs
        std::vector<std::set<int>> vs;
        stream >> value;
        auto vs_size = value;
        for(int i = 0; i < vs_size; ++i) { //vs
            std::set<int> s;
            stream >> value;
            auto s_size = value;
            for(int j = 0; j < s_size; ++j) { //s
                stream >> value;
                s.insert(value);
            }
            vs.push_back(s);
        }
        result.push_back(vs);
    }
    return result;
}

#endif // TASKSERIALIZER_H
