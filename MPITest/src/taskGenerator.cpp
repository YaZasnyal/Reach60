#include "taskGenerator.h"
#include "util.h"
#include "Checker.h"
#include "RecSubset.h"
#include "taskSerializer.h"

#include <future>
#include <thread>
#include <atomic>
#include <iostream>

std::set<std::set<int>> globalResult = std::set<std::set<int>>();
std::atomic<int> msgTransmitted(0);
int completedTasks = 0;
int totalTasks = 0;

std::vector<std::vector<std::set<int> > > createTask(std::vector<int> indexes, int n) {
    std::vector<std::vector<std::set<int>>> arrayReordered/*(n+1)*/;
    for(int j = 0; j <= n; ++j) {
        if(indexes[j] != 0) {
            auto elems = elemsOfOrderPlusK(j, n);
            RecSubset subsets(elems, indexes[j]);
            arrayReordered.push_back(subsets.getResult());
        }
    }
    return arrayReordered;
}

void runTaskCollector(volatile bool* cont) {
    try {
        std::packaged_task<void()> completedOutput([&](){
            while(completedTasks != totalTasks) {
                std::cout << "\r" << completedTasks << "/" << totalTasks;
                std::cout.flush();
                std::this_thread::sleep_for(std::chrono::nanoseconds(1000000000));
            }
        });
//        std::thread printer(std::move(completedOutput));

        taskCollector(cont);

//        printer.join();
    }
    catch(...) {
        std::cout<<"Exception in collector thread" << std::endl;
        exit(100);
    }
}

void taskCollector(volatile bool* cont) {
    while(*cont == true || msgTransmitted != 0) {
        std::vector<int> buf;

        ++completedTasks;
//        auto data = deserializeResult(buf);
//        for(auto& it : data) {
//            globalResult.insert(it);
//        }
        --msgTransmitted;
    }
}

void recCreate(int n, int rank, int size, int index) {
    static std::vector<int> v = constants(n);
    static std::vector<int> indexes(v.size());
    static int i = 0;
    if(index < v.size()) {
        for(int i = 0; i <= v[index]; ++i) {
            indexes[index] = i;
            recCreate(n, rank, size, index+1);
        }
        return;
    }
    //---------------------//
    if(i % size != rank) {
        ++i;
        ++msgTransmitted;
        return;
    }
    auto res = createTask(indexes, n);
    Checker c(res, n);

    static std::vector<int> buf(0);
    buf.clear();
    serializeResult(buf, c.getResult());
    ++i;
    ++msgTransmitted;
}

void caclTotalTasks(int n)
{
    recCreate(n, -1, 4);
    totalTasks = msgTransmitted;
    msgTransmitted = 0;
}
