#include "util.h"

#include <map>
#include <set>
#include <utility>
#include <mutex>

std::set<int> divisors(int n) {
    static std::mutex _m;
    static std::map<int, std::set<int>> container;
    //Если уже считали, то найдем и вернем значения
    {
        auto foundSet = container.find(n);
        if(foundSet != container.end()) {
            return (*foundSet).second;
        }
    }
    //Иначе посчитаем и добавим его в контейнер
    std::set<int> results;
    for(int i = 1; i <= n; ++i) {
        if( n % i == 0 ) {
            results.insert(i);
        }
    }
    std::lock_guard<std::mutex> g(_m);
    container[n] = results;

    return results;
}

std::set<int> coprimes(int n) {
    std::set<int> results;
    auto divisn = divisors(n);
    for(int i = 1; i <= n; ++i) {
        auto divisi = divisors(i);
        auto intersected = setIntersection(divisi, divisn);
        if(intersected.size() == 1) {
            results.insert(i);
        }
    }
    return results;
}

std::set<int> coprimesPlus(int n) {
    static std::mutex _m;
    static std::map<int, std::set<int>> container;
    {
        auto foundSet = container.find(n);
        if(foundSet != container.end()) {
            return (*foundSet).second;
        }
    }
    std::set<int> result;
    auto _coprimes = coprimes(n);
    for(auto it = _coprimes.begin(); it != _coprimes.end(); ++it) {
        if ((*it) <= n/2) {
            result.insert((*it));
        }
    }
    std::lock_guard<std::mutex> g(_m);
    container[n] = result;
    return result;
}

std::set<int> meta(const std::set<int>& s, int n) {
    std::set<int> result;
    for(auto it = s.begin(); it != s.end(); ++it) {
        result.insert((*it));
        result.insert(n-(*it));
    }
    return result;
}

std::set<int> mult(int a, const std::set<int>& s, int n) {
    std::set<int> result;
    for(auto it = s.begin(); it != s.end(); ++it) {
        result.insert((a * (*it)) % n);
//        result.insert((a * (n - (*it))) % n);
    }
    return result;
}

int order(int x, int n)
{
    static std::mutex _m;
    static std::map<int, int> container;
    {
        auto foundSet = container.find(x);
        if(foundSet != container.end()) {
            return (*foundSet).second;
        }
    }
    auto divs = divisors(n);
    for (auto& it : divs) {
        if((((it) * x) % n) == 0) {
            std::lock_guard<std::mutex> g(_m);
            container[x] = it;
            return (it);
        }
    }
    return -1;
}

std::set<int> elemsOfOrder(int k, const std::set<int> s, int n)
{
    std::set<int> result;
    for (auto& it : s) {
        if(order(it, n) == k) {
            result.insert(it);
        }
    }
    return result;
}

std::set<int> elemsOfOrderPlus(int k, const std::set<int> s, int n)
{
    auto s2 = setRange(n/2);
    return setIntersection(s2, elemsOfOrder(k, s, n));
}

/// @todo add cache for result
std::set<int> elemsOfOrderPlusK(int k, int n)
{
    std::set<int> s = setRange(n);
    auto res = elemsOfOrderPlus(k, s, n);
    return res;
}

std::vector<int> constants(int n) {
    static std::mutex _m;
    static std::map<int, std::vector<int>> container;
    {
        auto foundSet = container.find(n);
        if(foundSet != container.end()) {
            return (*foundSet).second;
        }
    }
    std::vector<int> result(n+1);
    for(int i = 1; i <= n; i++ ) {
        result[order(i, n)]++;
    }
    /** @todo add fix for odd numbers */
    for(int i = 0; i < result.size(); ++i) {
        result[i] = result[i] == 1 ? 1 : result[i]/2;
    }

    //even number improvements
    if(n % 2 == 0) {
        result[2] = 0;
    }

    std::lock_guard<std::mutex> g(_m);
    container[n] = result;
    return result;
}

bool setCompare(const std::set<int>& first, const std::set<int>& last, int n, bool debug)
{
    for(auto& it : coprimesPlus(n)) {
        bool result = true;
        for(auto& jt : first) {
            int val = (it * jt) % n;
            if(last.find(val) == last.end() && last.find(n-val) == last.end()) {
                result = false;
                break;
            }
        }
        if(result) {
            return true;
        }
    }
    return false;
}

std::set<int> setRange(int n)
{
    std::set<int> result;
    for(int i = 1; i <= n; ++i) {
        result.insert(i);
    }
    return result;
}
