#ifndef CONSUMER_H
#define CONSUMER_H

#include "Net/TaskRunnable.h"
#include <atomic>
#include <mutex>

class Consumer : public TaskRunnable
{
    Q_OBJECT

public:
    Consumer() : TaskRunnable() {
    }

    virtual void check() override;

private:
    static std::atomic<int> completedTasks;
    static std::mutex _coutblock;
};

#endif // CONSUMER_H
