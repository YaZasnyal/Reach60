#ifndef RECSUBSET_H
#define RECSUBSET_H

#include <set>
#include <vector>

/**
 * @brief The RecSubset class forms all possible combinations of sets of lenght
 * k using elemets from input set.
 *
 * @code
 * std::set<int> s;
 * s.insert({1,2,3,4});
 *
 * RecSubset rs(s, 3);
 * //print rs.getResult();
 * //1 2 3
 * //1 2 4
 * //1 3 4
 * //2 3 4
 * @endcode
 */
class RecSubset
{
public:
    RecSubset(const std::set<int>& s, int k);

    std::vector<std::set<int> > getResult() const;

    void subsets(std::set<int>::const_iterator sBegin,
                 const std::set<int>::const_iterator& sEnd,
                 int k,
                 std::set<int> nextRes = std::set<int>(),
                 int index = 0,
                 int enabled = 0);

private:
    std::vector<std::set<int>> result;
};

#endif // RECSUBSET_H
