#include "TaskRunnable.h"
#include <QThread>

TaskRunnable::TaskRunnable() : QObject(), QRunnable()
{
    setAutoDelete(true);
}

int TaskRunnable::getTaskId() const
{
    return taskId;
}

void TaskRunnable::setTaskId(int value)
{
    taskId = value;
}

void TaskRunnable::setInputData(QByteArray& value)
{
    inputData.swap(value);
}

void TaskRunnable::run()
{
    QThread::currentThread()->setPriority(QThread::IdlePriority);
    check();
    emit finished(data);
}
