#include "Server.h"
#include "ServerConnection.h"

Server::Server(QObject *parent) : QTcpServer(parent)
{
    connect(this, SIGNAL(newConnection()), this, SLOT(newClient()));
    qDebug() << "Server: " << listen(QHostAddress::Any, 10241);
}

void Server::newClient()
{
//    qDebug()<<"newConnection";
    while(hasPendingConnections()) {
        auto connection = nextPendingConnection();
        ServerConnection *s = new ServerConnection(connection, this);
        connect(s, SIGNAL(taskRequested(int)), this, SLOT(taskRequested(int)));
        connect(s, SIGNAL(resultRecieved(QByteArray)), this, SIGNAL(resultRecieved(QByteArray)));
        connect(s, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    }
}

void Server::taskRequested(int num)
{
    auto sock = qobject_cast<ServerConnection*>(sender());
    if(sock == nullptr) {
        return;
    }
    bool ok;
    for(int i = 0; i < num; ++i) {
        auto task = dequeue(&ok);
        if(ok)
            sock->sendNewTask(task);
        else {
            sock->sendCommand(NoTasks);
            return;
        }
    }
}

//void Server::resultRecieved(QByteArray data)
//{
//    qDebug() << "m.data" << data;
//}

void Server::socketDisconnected()
{
    auto sock = qobject_cast<ServerConnection*>(sender());
    if(sock == nullptr) {
        return;
    }
    for(auto& it : sock->getSendedMessages()) {
        enqueue(it, true);
    }
    sock->deleteLater();
}
