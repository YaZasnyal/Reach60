#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QByteArray>
#include <QDataStream>
#include <iostream>

#include <QList>
#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>

#define BUFFER_LENGTH 200

class Server : public QTcpServer {
    Q_OBJECT

public:
    explicit Server(QObject *parent = 0);

    void enqueue(QByteArray data, bool force = false ) {
        _queueMutex.lock();
        while(queue.length() >= BUFFER_LENGTH && !force) {
            _queueCanAdd.wait(&_queueMutex);
        }
        queue.append(data);
        _queueMutex.unlock();
        queueNotEmpty = true;
        _queueAdded.wakeOne();
    }

    QByteArray dequeue(bool* ok) {
        if(!queueNotEmpty) {
            *ok = false;
            return QByteArray();
        }
        QMutexLocker locker(&_queueMutex);
        while(queue.length() == 0) {
            *ok = _queueAdded.wait(&_queueMutex, 1000);
            if(!*ok) {
                queueNotEmpty = false;
                return QByteArray();
            }
        }
        auto res = queue.first();
        queue.pop_front();
        locker.unlock();
        _queueCanAdd.wakeOne();

        *ok = true;
        return res;
    }

signals:
    void resultRecieved(QByteArray data);

private slots:
    void newClient();
    void taskRequested(int num);
//    void resultRecieved(QByteArray data);
    void socketDisconnected();

private:
    QList<QByteArray> queue;
    QMutex _queueMutex;
    QWaitCondition _queueCanAdd;
    QWaitCondition _queueAdded;

    bool queueNotEmpty = true;
};

#endif // SERVER_H
