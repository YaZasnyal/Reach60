#ifndef MESSAGE_H
#define MESSAGE_H

#include <atomic>
#include <QByteArray>
#include <QDataStream>

class Message
{
public:
    Message();
    Message(int command);
    Message(QByteArray& data);
    Message(int command, QByteArray& data);

    QByteArray encode() const;
    void decode(QByteArray& data);


    int command() const;
    void setCommand(int command);

    QByteArray& data();
    void setData(QByteArray& data);

    int getTaskId() const;
    void setTaskId(int value);

private:
    int _command;
    int TaskId = -1;
    QByteArray _data;
};

#endif // MESSAGE_H
