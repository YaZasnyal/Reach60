#include "Message.h"

Message::Message()
{

}

Message::Message(int command) {
    _command = command;
}

Message::Message(QByteArray& data) {
    data.swap(_data);
}

Message::Message(int command, QByteArray& data) {
    _command = command;
    data.swap(_data);
}

QByteArray Message::encode() const {
    QByteArray block;
    QDataStream str(&block, QIODevice::WriteOnly);
    str.setVersion(QDataStream::Qt_4_0);
    str << _command << TaskId << _data;

    return block;
}

void Message::decode(QByteArray& data) {
    QDataStream str(&data, QIODevice::ReadOnly);
    str.setVersion(QDataStream::Qt_4_0);

    str >> _command >> TaskId >> _data;
}

int Message::command() const
{
    return _command;
}

void Message::setCommand(int command)
{
    _command = command;
}

QByteArray& Message::data()
{
    return _data;
}

void Message::setData(QByteArray& data)
{
    _data.swap(data);
}

int Message::getTaskId() const
{
    return TaskId;
}

void Message::setTaskId(int value)
{
    TaskId = value;
}
