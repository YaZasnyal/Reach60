#ifndef SERVERCONNECTIONTHREAD_H
#define SERVERCONNECTIONTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDataStream>
#include <QHostAddress>
#include <Net/Message.h>

class Connection : public QObject
{
    Q_OBJECT

public:
    Connection(QTcpSocket* socket, QObject *parent);
    bool writeData(QByteArray data);
    void SendMessage(const Message& m);

    void connectToHost(QString addr, qint16 port);
    void disconnectFromHost();

private slots:
    void readyRead();
    void socketError(QAbstractSocket::SocketError);

signals:
    void stateChanged(QAbstractSocket::SocketState state);
    void dataRecieved(QByteArray data);
    void connected();
    void disconnected();

private:
    QTcpSocket *_socket;
    QHostAddress _peerAddr;
    qint16 _peerPort;
    QDataStream in;
};

#endif // SERVERCONNECTIONTHREAD_H
