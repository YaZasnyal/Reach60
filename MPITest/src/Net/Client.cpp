#include "Client.h"

#include <QtNetwork>
#include "Message.h"
#include "CommandsConst.h"
#include "util.h"
#include "Checker.h"
#include "taskSerializer.h"

Client::Client(std::function<TaskRunnable*()> factory, QObject *parent) : QObject(parent),
    _factory(factory)
{
    qRegisterMetaType<TaskResultData>("TaskResultData");

    QTcpSocket *sock = new QTcpSocket();
    sock->connectToHost(_peerAddr, _peerPort);
//    sock->waitForConnected();

    qDebug() << sock->peerAddress() << " : " << sock->peerPort();
    _pool.setExpiryTimeout(-1);
    qDebug()<< "maxThreadCount: " << _pool.maxThreadCount();

    connection = new Connection(sock, this);
    connect(connection, SIGNAL(dataRecieved(QByteArray)), this, SLOT(dataRecieved(QByteArray)));
    connect(connection, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketStateChanged(QAbstractSocket::SocketState)));
}

void Client::getMoreTasks_slt(int n) {
    if(!tasksAvailable) return;
    Message m;
    m.setCommand(ClientRequestTask);
    QByteArray ba;
    QDataStream str(&ba, QIODevice::WriteOnly);
    str << n;
    m.setData(ba);
    connection->writeData(m.encode());
}

void Client::dataRecieved(QByteArray data) {
    Message m;
    m.decode(data);

    switch (m.command()) {
    case TaskData: {
        ++tasks;
        TaskRunnable* task = _factory();
        task->setInputData(m.data());
        task->setTaskId(m.getTaskId());
//        qDebug()<< "Recieved " << m.getTaskId();
        connect(task, SIGNAL(finished(TaskResultData)), this, SLOT(taskFinished(TaskResultData)));
        _pool.start(task);

        break;
    }
    case NoTasks: {
//        qDebug() << "NoTasks recieved";
        tasksAvailable = false;
        break;
    }
    default:
        break;
    }
    tasksCountChanged();
}

void Client::taskFinished(TaskResultData data)
{
    --tasks;
    tasksCountChanged();
    Message m;
    m.setCommand(ResultData);
    m.setTaskId(data.taskId);
    m.setData(data.outputData);
    connection->SendMessage(m);
}

void Client::socketStateChanged(QAbstractSocket::SocketState state)
{
    qDebug() << state;
    switch (state) {
    case QAbstractSocket::UnconnectedState:
//        qDebug() << "Reconnecting";
        _pool.clear();
        tasks = 0;
        QTimer::singleShot(1000, this, SLOT(connectHost()));
        break;
    case QAbstractSocket::ListeningState:
        tasksCountChanged();
        break;
    case QAbstractSocket::ConnectedState:
        tasksAvailable = true;
        tasksCountChanged();
        break;
    default:
        break;
    }
}

void Client::connectHost()
{
    connection->connectToHost(_peerAddr, _peerPort);
}

void Client::disconnect()
{
    connection->disconnect();
}

void Client::tasksCountChanged()
{
//    qDebug() << "tasks: " << tasks;
    if(tasks < QThread::idealThreadCount() * bufferCoeff) {
        getMoreTasks_slt(QThread::idealThreadCount() * bufferCoeff - tasks);
    }
}

qint16 Client::getPeerPort() const
{
    return _peerPort;
}

void Client::setPeerPort(const qint16& peerPort)
{
    _peerPort = peerPort;
}

void Client::setThreadCount(int n)
{
    if(n < 0)
        _pool.setMaxThreadCount(QThread::idealThreadCount());
    else
        _pool.setMaxThreadCount(n == 0 ? 1 : n);
}

QString Client::getPeerAddr() const
{
    return _peerAddr;
}

void Client::setPeerAddr(const QString& peerAddr)
{
    _peerAddr = peerAddr;
}
