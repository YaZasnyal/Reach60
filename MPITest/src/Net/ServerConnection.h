#ifndef SERVERCONNECTION_H
#define SERVERCONNECTION_H

#include "Connection.h"
#include "CommandsConst.h"

class ServerConnection : public Connection
{
    Q_OBJECT

public:
    ServerConnection(QTcpSocket* s, QObject *parent = 0);
    void sendNewTask(QByteArray data);
    void sendCommand(Commands command);
    void sendCommand(Commands command, QByteArray data, int id = -1);

    QMap<int, QByteArray>& getSendedMessages();

signals:
    void taskRequested(int i);
    void resultRecieved(QByteArray);

private slots:
    void dataRecieved_slt(QByteArray data);

private:
    QMap<int, QByteArray> sendedMessages;
    int taskId = 0;
};

#endif // SERVERCONNECTION_H
