#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>
#include <QCoreApplication>

inline QString getArg(const QString& arg, const QString& defVal) {
    auto args = QCoreApplication::arguments();
    int id = args.indexOf(arg);
    if(id == -1) {
        return defVal;
    }
    return args[id+1];
}

#endif // GLOBAL_H
