#include "ServerConnection.h"

#include <iostream>
#include "Message.h"

ServerConnection::ServerConnection(QTcpSocket* s, QObject *parent) :
    Connection(s, parent)
{
    connect(this, SIGNAL(dataRecieved(QByteArray)), this, SLOT(dataRecieved_slt(QByteArray)));
}

void ServerConnection::sendNewTask(QByteArray data)
{
    auto id = ++taskId;
    sendedMessages[id] = data;
    sendCommand(TaskData, data, id);
}

void ServerConnection::sendCommand(Commands command)
{
    sendCommand(command, QByteArray());
}

void ServerConnection::sendCommand(Commands command, QByteArray data, int id)
{
    Message m(command, data);
    m.setTaskId(id);
    writeData(m.encode());
}

void ServerConnection::dataRecieved_slt(QByteArray data)
{
    Message m;
    m.decode(data);

    switch (m.command()) {
    case ClientRequestTask: {
        int i;
        QDataStream str(&m.data(), QIODevice::ReadOnly);
        str >> i;
        emit taskRequested(i);
        break;
    }
    case ResultData: {
        if(!sendedMessages.contains(m.getTaskId())) {
            break;
        }
        sendedMessages.remove(m.getTaskId());
        emit resultRecieved(m.data());
        break;
    }
    default:
        std::cout << "Unchecked command: " << m.command() << "\n";
        break;
    }
}

QMap<int, QByteArray>& ServerConnection::getSendedMessages()
{
    return sendedMessages;
}
