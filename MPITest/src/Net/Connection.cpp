#include "Connection.h"

Connection::Connection(QTcpSocket *socket, QObject* parent) :
    QObject(parent),
    _socket(socket)
{
    _socket->setParent(this);
    _peerAddr = _socket->peerAddress();
    _peerPort = _socket->peerPort();

    connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    connect(_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    connect(_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SIGNAL(stateChanged(QAbstractSocket::SocketState)));
    connect(_socket, SIGNAL(connected()), this, SIGNAL(connected()));
    connect(_socket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));

    in.setDevice(_socket);
    in.setVersion(QDataStream::Qt_4_0);
}

bool Connection::writeData(QByteArray data) {
    if(_socket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray block;
        QDataStream str(&block, QIODevice::WriteOnly);
        str.setVersion(QDataStream::Qt_4_0);

        str << data;

        _socket->write(block);
        return true;
    }
    else {
        return false;
    }
}

void Connection::SendMessage(const Message& m)
{
    writeData(m.encode());
}

void Connection::connectToHost(QString addr, qint16 port)
{
    qDebug() << addr << " : " << port;
    _socket->connectToHost(addr, port, QIODevice::ReadWrite);
}

void Connection::disconnectFromHost()
{
    _socket->disconnectFromHost();
}

void Connection::readyRead()
{
    while(true) {
        //        qDebug() << _socket->bytesAvailable();
        in.startTransaction();

        QByteArray buf;
        in >> buf;

        if (!in.commitTransaction())
            return;

        emit dataRecieved(buf);
    }
}

void Connection::socketError(QAbstractSocket::SocketError err)
{
    qDebug() << err;
}
