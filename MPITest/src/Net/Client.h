#ifndef CLIENT_H
#define CLIENT_H

#include <functional>
#include <QObject>
#include "Connection.h"
#include <QWaitCondition>
#include <QThreadPool>

#include <Net/AbstractFactory.h>
#include "Net/TaskRunnable.h"

class Client : public QObject
{
    Q_OBJECT

public:
    Client(std::function<TaskRunnable*()> factory, QObject *parent = 0);

    QString getPeerAddr() const;
    void setPeerAddr(const QString& peerAddr);

    qint16 getPeerPort() const;
    void setPeerPort(const qint16& peerPort);

    void setThreadCount(int n);

signals:
    void getMoreTasks();

private slots:
    void getMoreTasks_slt(int n);
    void dataRecieved(QByteArray data);
    void taskFinished(TaskResultData data);
    void socketStateChanged(QAbstractSocket::SocketState state);

public slots:
    void connectHost();
    void disconnect();

private:
    void tasksCountChanged();

    std::function<TaskRunnable*()> _factory;

    QString _peerAddr = "localhost";
    qint16 _peerPort = 10241;

    int tasks = 0;
    int bufferCoeff = 2;
    Connection *connection;
    QThreadPool _pool;
    volatile bool tasksAvailable = true;
};

#endif // CLIENT_H
