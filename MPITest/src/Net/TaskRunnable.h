#ifndef TASKRUNNABLE_H
#define TASKRUNNABLE_H

#include <QMetaType>
#include <QObject>
#include <QRunnable>


class TaskResultData {
public:
    int taskId;
    QByteArray outputData;
};

class TaskRunnable : public QObject, public QRunnable
{
    Q_OBJECT

public:
    TaskRunnable();

    int getTaskId() const;
    void setTaskId(int value);
    void setInputData(QByteArray& value);

    void run() override;

    virtual void check() = 0;

signals:
    void finished(TaskResultData);

protected:
    int& taskId = data.taskId;
    QByteArray inputData;
    QByteArray& outputData = data.outputData;

private:
    TaskResultData data;
};

#endif // TASKRUNNABLE_H
