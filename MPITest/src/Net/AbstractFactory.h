#ifndef TASKRUNNABLEFACTORY_H
#define TASKRUNNABLEFACTORY_H

#include <functional>

template <class T>
std::function<T*()> Factory() {
    std::function<T*()> f = []()->T*{
        return new T();
    };
    return f;
}

#endif // TASKRUNNABLEFACTORY_H
