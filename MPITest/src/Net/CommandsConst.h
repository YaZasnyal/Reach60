#ifndef COMMANDSCONST_H
#define COMMANDSCONST_H

enum Commands {
    TaskData = 1,
    ResultData = 2,
    NoTasks = 3,


    ClientPause = 10,
    ClientResume = 11,
    ClientRequestTask = 12
};

#endif // COMMANDSCONST_H
