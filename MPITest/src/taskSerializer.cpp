#include "taskSerializer.h"

std::vector<std::vector<std::set<int> > > deserialize(const std::vector<int>& buf) {
    std::vector<std::vector<std::set<int>>> result;
    int pos = 0;
    auto vvs_size = buf[pos++];
    for(int k = 0; k < vvs_size; ++k) { //vvs
        std::vector<std::set<int>> vs;
        auto vs_size = buf[pos++];
        for(int i = 0; i < vs_size; ++i) { //vs
            std::set<int> s;
            auto s_size = buf[pos++];
            for(int j = 0; j < s_size; ++j) { //s
                s.insert(buf[pos++]);
            }
            vs.push_back(s);
        }
        result.push_back(vs);
    }
    return result;
}

void serializeResult(std::vector<int>& buf, std::set<std::set<int> > s) {
    buf.push_back(s.size());
    for(auto& it : s) {
        buf.push_back(it.size());
        for(auto& jt : it) {
            buf.push_back(jt);
        }
    }
}

std::set<std::set<int>> deserializeResult(const std::vector<int>& buf)
{
    std::set<std::set<int>> result;
    int pos = 0;
    auto ss_size = buf[pos++];
    for(int k = 0; k < ss_size; ++k) { //ss
        std::set<int> s;
        auto s_size = buf[pos++];
        for(int k = 0; k < s_size; ++k) { //ss
            s.insert(buf[pos++]);
        }
        result.insert(s);
    }
    return result;
}

void serializeTask(std::vector<int>& buf, const std::vector<std::vector<std::set<int> > >& s) {
    buf.push_back(s.size());
    for(auto& it : s) {
        buf.push_back(it.size());
        for(auto& jt : it) {
            buf.push_back(jt.size());
            for(auto& kt : jt) {
                buf.push_back(kt);
            }
        }
    }
}

template<class T>
void serializeTask(T &stream, const std::vector<std::vector<std::set<int> > > &s) {
    stream << static_cast<int>(s.size());
    for(auto& it : s) {
        stream << static_cast<int>(it.size());
        for(auto& jt : it) {
            stream << static_cast<int>(jt.size());
            for(auto& kt : jt) {
                stream << kt;
            }
        }
    }
}


