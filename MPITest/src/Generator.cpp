#include "Generator.h"
#include "taskGenerator.h"
#include "taskSerializer.h"
#include "util.h"

#include <QDataStream>
#include <QDebug>
#include <QByteArray>
#include "Net/Server.h"

Generator::Generator(Server* s, int n, QObject *parent) :
    QThread(parent),
    _s(s),
    _n(n)
{
    init();
    recCreate(_n, 0, true);
    init();
}

Generator::Generator(Server* s, int n, int start, int end, QObject* parent)
{
    _start = start;
    _end = end;
    _n = n;
    _s = s;
    init();
    recCreate(_n, 0, true);
    init();
}

void Generator::init()
{
    v = constants(_n);
    indexes.clear();
    indexes.resize(v.size());
}

void Generator::run()
{
    qDebug() << "Total number of tasks: " << totalTasks;
    this->recCreate(_n);
}

void Generator::recCreate(int n, int index, bool check) {
    if(index < v.size()) {
        for(int i = 0; i <= v[index]; ++i) {
            indexes[index] = i;
            Generator::recCreate(n, index+1, check);
        }
        return;
    }
    //---------------------//
    if(check) {
        ++totalTasks;
        return;
    }

    static int currentTaskId = 0;
    if(_start != -1 && _end != -1) {
        if(currentTaskId < _start || currentTaskId >= _end) {
            ++currentTaskId;
            return;
        }
    }

    auto res = createTask(indexes, n);

    std::vector<int> buf;
    serializeTask(buf, res);

    QByteArray ba;
    QDataStream str(&ba, QIODevice::WriteOnly);
    str << currentTaskId++ << n;
    for(auto& it : buf) {
        str << it;
    }

    _s->enqueue(ba);
//    c.writeData(ba);
}

int Generator::getN() const
{
    return _n;
}

int Generator::getTotalTasks() const
{
    return totalTasks;
}
