#include "Collector.h"
#include <QDataStream>

#include <iostream>
#include <iomanip>      // std::setprecision

#include "Net/global.h"
#include "util.h"
#include "taskSerializer.h"

Collector::Collector(int ntasks, int n, QObject* parent) :
    QObject(parent),
    _ntasks(ntasks),
    _n(n)
{
    f = new QFile("out.txt", this);
    f->open(QIODevice::WriteOnly);

    connect(&timer, SIGNAL(timeout()), this, SLOT(printProgress()));
    timer.start(1000);
    elapsedTimer.start();

    qDebug() << "Opened file: " << f->isOpen();
    str = new QTextStream(f);

    //Second pack
    _printFull = (getArg("-full", "false") == "true" && _n%2==0);
}

void Collector::recievedData(QByteArray data)
{
    ++writtenTasks;
    QDataStream stream(&data, QIODevice::ReadOnly);
    int currentTaskId;
    stream >> currentTaskId;
    auto res = deserializeResult(stream);

    for(auto& it : res) {
        printContainer(*str, it);
    }
    if(_printFull) {
        for(auto& it : res) {
            printContainer(*str, it, false);
            *str << _n/2 << "\n";
        }
    }
    if(_ntasks != -1 && _ntasks == writtenTasks) {
        str->flush();
        f->close();
        exit(0);
    }
}

void Collector::printProgress()
{
    static std::vector<int> progressData(120);
    static unsigned int index = 0;
    static int previousWrittenTsks = 0;
    progressData[++index%progressData.size()] = writtenTasks - previousWrittenTsks;
    previousWrittenTsks = writtenTasks;
    double avg = 0;
    for(auto& it : progressData) {
        avg += it;
    }
    avg = avg/progressData.size();

    double percentage = static_cast<double>(writtenTasks)/_ntasks*100;
    int ncols  = 40;
    std::cout << "\r[";
    for(int i = 0; i < ncols; ++i) {
        if(100./ncols*i < percentage)
            std::cout << "#";
        else
            std::cout << " ";
    }
    std::cout << "] " << writtenTasks <<"/"<<_ntasks<<"(" << std::setprecision(4) << percentage <<"%)";

    int secs = elapsedTimer.elapsed() / 1000;
    int mins = (secs / 60) % 60;
    int hours = (secs / 3600);
    std::cout << " Elapsed: " << hours<<":"<<mins<<":"<<(secs%60);
    std::cout << " TPHour = " << std::setprecision(2) << avg*60*60;

    double etaSec = (avg == 0 ? -1 : static_cast<double>(_ntasks-writtenTasks)/avg);
    double etaHours = (etaSec / 3600);

    if(etaSec > 0) {
        std::cout << " ETA = " << std::setprecision(2) << etaHours <<"h            ";
    }
    std::cout.flush();
}
