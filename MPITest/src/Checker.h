#ifndef CHECKER_H
#define CHECKER_H

#include <set>
#include <vector>

class Checker {
public:
    Checker(const std::vector<std::vector<std::set<int>>>& vs, int n /*= 60*/);
    int _n;

    std::set<std::set<int> > getResult() const;

private:
    void check(const std::vector<std::vector<std::set<int>>>& vs,
               int depth = 0,
               std::set<int> newSet = std::set<int>());

    std::set<std::set<int>> result;
};

#endif // CHECKER_H
