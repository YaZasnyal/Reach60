#include "Checker.h"
#include "util.h"

Checker::Checker(const std::vector<std::vector<std::set<int> > >& vs, int n) {
    this->_n = n;
    check(vs);
}

void Checker::check(const std::vector<std::vector<std::set<int> > >& vs, int depth, std::set<int> newSet)
{
    if (depth < vs.size()) {
        for(auto& it : vs[depth]) {
            auto newSetCopy = newSet;
            for(auto& jt : it) {
                newSetCopy.insert(jt);
            }
            check(vs, depth+1, newSetCopy);
        }
        return;
    }
    //-------------------//
//    newSet = meta(newSet, _n);
    if(result.size() == 0) {
        result.insert(newSet);
    } else {
        bool shouldBeAdded = true;
        for(auto& it : result) {
            if(setCompare(it, newSet, _n)) {
                shouldBeAdded = false;
                return;
            }
        }
        if (shouldBeAdded) {
            result.insert(newSet);
        }

    }
}

std::set<std::set<int> > Checker::getResult() const
{
    return result;
}
