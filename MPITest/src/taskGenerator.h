#ifndef TASKGENERATOR_H
#define TASKGENERATOR_H

#include <vector>
#include <set>
#include <atomic>

extern std::set<std::set<int>> globalResult;
extern int totalTasks;
extern std::atomic<int> msgTransmitted;

std::vector<std::vector<std::set<int>>> createTask(std::vector<int> indexes, int n /*= 60*/);
void recCreate(int n /*= 60*/, int rank, int size, int index = 0);

void runTaskCollector(volatile bool* cont);
void taskCollector(volatile bool* cont);

void caclTotalTasks(int n);

#endif // TASKGENERATOR_H
