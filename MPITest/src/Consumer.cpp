#include "Consumer.h"

#include <iostream>

#include <QDataStream>

#include "util.h"
#include "taskSerializer.h"
#include "Checker.h"

std::atomic<int> Consumer::completedTasks(0);
std::mutex Consumer::_coutblock;

void Consumer::check() {
    QDataStream stream(&inputData, QIODevice::ReadOnly);
    int n;
    int currentTaskId;
    stream >> currentTaskId >> n;
    auto task = deserializeTask(stream);

    Checker c(task, n);

    QByteArray ba;
    QDataStream ostream(&ba, QIODevice::WriteOnly);
    ostream << currentTaskId;
    serializeResult(ostream, c.getResult());

    outputData = ba;

    _coutblock.lock();
    std::cout << "\rCompleted tasks: " << ++completedTasks <<"    ";
    std::cout.flush();
    _coutblock.unlock();
}
