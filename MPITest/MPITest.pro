QT += core network
QT -= gui
CONFIG += console c++14

SOURCES += main.cpp \
    src/Checker.cpp \
    src/RecSubset.cpp \
    src/taskGenerator.cpp \
    src/taskSerializer.cpp \
    src/util.cpp \
    src/Net/Server.cpp \
    src/Net/Client.cpp \
    src/Generator.cpp \
    src/Consumer.cpp \
    src/Net/Message.cpp \
    src/Net/Connection.cpp \
    src/Net/ServerConnection.cpp \
    src/Collector.cpp \
    src/Net/TaskRunnable.cpp

unix {
    LIBS += -L/usr/local/Cellar/gperftools/2.7/lib/ -ltcmalloc_minimal
}

INCLUDEPATH += \
    src

HEADERS += \
    src/Checker.h \
    src/RecSubset.h \
    src/taskGenerator.h \
    src/taskSerializer.h \
    src/util.h \
    src/Net/Server.h \
    src/Net/Client.h \
    src/Generator.h \
    src/Consumer.h \
    src/Net/Message.h \
    src/Net/CommandsConst.h \
    src/Net/Connection.h \
    src/Net/ServerConnection.h \
    src/Collector.h \
    src/Net/TaskRunnable.h \
    src/Net/AbstractFactory.h \
    src/Timer.h \
    src/Net/global.h
