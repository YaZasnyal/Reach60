TEMPLATE = subdirs

SUBDIRS = MPITest/MPITest.pro \
    Reach60Client \
    Converter \
    resultMerger \
    oddCirculants \
    Tests \
    Isororphism
