#include <QCoreApplication>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    auto args = a.arguments();
    if(args.length() < 3) {
        qDebug() << "Too little arguments. firmat <input> <output>";
        exit(1);
    }

    QFile in(args[1]);
    in.open(QIODevice::ReadOnly);
    if(!in.isOpen()) {
        qDebug() << "Couldn`t open input file";
    }
    QFile out(args[2]);
    out.open(QIODevice::WriteOnly);
    if(!out.isOpen()) {
        qDebug() << "Couldn`t open output file";
    }

    QTextStream inputStream(&in);
    QTextStream outputStream(&out);

    outputStream << '{';
    while(!inputStream.atEnd()) {
        auto line = inputStream.readLine().trimmed();
        line.prepend('{');
        for(int i = 0; i < line.length() - 1; ++i) {
            if(line.at(i) == ' ') {
                line[i] = ',';
            }
        }
        line.append('}');
        if(!inputStream.atEnd()) {
          line.append(',');
        }
        outputStream << line;
    }
    outputStream << '}';

    outputStream.flush();
    out.flush();
    out.close();
    qDebug() << "Finished!";
    exit(0);

    return a.exec();
}
