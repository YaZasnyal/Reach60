#!/bin/bash
FOLDER=$1

NUMBER_OF_FILES=`\ls -afq $FOLDER | wc -l`
echo $NUMBER_OF_FILES

for (( i=0; i <= $NUMBER_OF_FILES; i++ ))
do
  ./Converter.exe $FOLDER/$i.txt $FOLDER/"$i"_.txt
  rm $FOLDER/$i.txt
done